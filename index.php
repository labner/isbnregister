<?php
require_once('functions.php');
session_start();
connect_db();
include_once('views/head.html');
$mode="main";

if(!empty($_GET['mode'])) {
	$mode=$_GET['mode'];
}

switch($mode){
	case "login":
		login();
	break;
	case "logout":
		logout();
	break;
	case "isbn":
		taotleisbn();
	break;
	case "vaata":
		vaata();
	break;
	case "kirjastaja":
		kirjastaja();
	break;
	case "uuskirjastaja":
		uuskirjastaja();
	break;
	case "annaplokk":
		annaplokk();
	break;
	default:
		include_once("views/main.html");
	break;
}
include_once('views/foot.html');
?>