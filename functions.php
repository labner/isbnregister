<?php
function connect_db(){
	global $connection;
	$host="localhost";
	$user="test"; // PS! on ebaturvaline hoida avalikus gitis ab kasutajat/parooli
	$pass="t3st3r123";
	$db="test";
	$connection = mysqli_connect($host, $user, $pass, $db) or die("MySQL ühendus feilis ".mysqli_error());
	mysqli_query($connection, "SET NAMES 'utf8'") or die("UTF-8 feilis ".mysqli_error($connection));
}

function login(){
	global $connection;
	//echo "<pre>";
	//print_r($_SERVER);
	//echo "</pre>";
	//kontrollib, kas on POST meetodil päring, st kasutaja on vormi saatnud
	if ($_SERVER['REQUEST_METHOD'] == "POST") {
		if (isset($_POST['kasutajanimi']) && isset($_POST['parool'])) {
			//kontrollib, kas ni kasutajanimi kui parool on saadetud
			if ($_POST['kasutajanimi'] == "" || $_POST['parool'] == "") {
				$errors[] = "Kasutajanimi või parool on tühi";
			} else {
				//muudame kahjutuks kasutaja poolt sisestatud html-i ja sql-i
				$kasutajanimi = mysqli_real_escape_string($connection,htmlspecialchars($_POST['kasutajanimi']));
				$parool = mysqli_real_escape_string($connection,htmlspecialchars($_POST['parool']));
				//vaatame, kas sellise kasutajanime ja parooliga kasutaja eksisteerib
				//hea praktika on kasutada prepared statemente
				//tegelikult ei ole prepared statementi puhul mysqli_real_escape_string vajalik
				$stmt = $connection->prepare("SELECT * FROM labner_kasutaja WHERE kasutajanimi=? AND parool=SHA1(?)");
				$stmt->bind_param("ss", $kasutajanimi,$parool); //ss - string string, st mõlemad parameetrid on stringid
				$stmt->execute() or die ("Ei saanud kasutaja päringut teha.");
				$result = $stmt->get_result();
				//kui on täpselt üks kasutaja, siis kirjutame sessiooni vastavad parameetrid
				//vastasel juhul - viga
				if (mysqli_num_rows($result) == 1) {
					$row =mysqli_fetch_assoc($result);
					//print_r($row);
					$id = $row['id'];
					$roll = $row['roll'];
					$perenimi = $row['perenimi'];
					$eesnimi = $row['eesnimi'];
					$_SESSION['id'] = $id;
					$_SESSION['kasutajanimi'] = $kasutajanimi;
					$_SESSION['roll'] = $roll;
					//kui roll on 0 st. kirjastaja, siis lisame ka kirjastaja andmed sessioonile
					//kirjastaja suuname isbn taotlema
					//admini suuname kirjastajaid vaatama
					if ($roll == 0) {
						getkirjastajaseos($id);
						header("Location: main.php?mode=isbn");
						exit();
					}
					if ($roll == 1) {
						header("Location: main.php?mode=kirjastaja");
						exit();
					}
				} else {
					$errors[] = "Kasutajanimi või parool on vale";
				}
			}
		}			
	}
	include_once('views/login.html');
}

function logout(){
	$_SESSION=array();
	session_destroy();
	header("Location: ?");
}
function getkirjastajaseos($kirjastaja_id) {
	global $connection;
	//millise kirjastajaga seotud on
	$stmt = $connection->prepare("SELECT * FROM labner_kirjastaja_kasutaja AS kk LEFT JOIN labner_kirjastaja AS k ON kk.kirjastaja_id = k.id WHERE kk.kasutaja_id=?");
	$stmt->bind_param("i", $_SESSION['id']); //i - int
	$stmt->execute() or die ("Ei saanud kasutaja-kirjastaja päringut teha.");
	$result = $stmt->get_result();
	if (mysqli_num_rows($result) >= 1) {
		$row = mysqli_fetch_assoc($result);
		//print_r($row);
		$_SESSION['kirjastaja'] = $row;
		getplokk($_SESSION['kirjastaja']['kirjastaja_id']);
	}
}
function getkirjastaja ($kirjastaja_id) {
	global $connection;
	//kas on olemas
	$stmt = $connection->prepare("SELECT * FROM labner_kirjastaja WHERE id=?");
	$stmt->bind_param("i", $kirjastaja_id); //i - int
	$stmt->execute() or die ("Ei saanud kirjastaja päringut teha.");
	$result = $stmt->get_result();
	if (mysqli_num_rows($result) == 1) {
		$kirjastaja = mysqli_fetch_assoc($result);
		//print_r($kirjastaja);
		return $kirjastaja;
	}
}
function vaata() {
	global $connection;
	if (!isset($_SESSION['id'])) {
		header("Location: main.php?mode=login");
		exit();
	}
	if (isset($_SESSION['kirjastaja']['kirjastaja_id'])) {
		$kirjastaja_id = $_SESSION['kirjastaja']['kirjastaja_id'];
	} else {
		$kirjastaja_id = htmlspecialchars($_GET['kirjastaja_id']);
	}
	
	$stmt = $connection->prepare("SELECT * FROM labner_raamat_kirjastaja rk LEFT JOIN labner_raamat r ON rk.raamat_id = r.id LEFT JOIN labner_isbn i ON r.id = i.raamat_id WHERE rk.kirjastaja_id = ?");
	$stmt->bind_param("i", $kirjastaja_id);
	$stmt->execute() or die ("Ei saanud raamatu päringut teha.");
	$result = $stmt->get_result();
	if (mysqli_num_rows($result) >= 1) {
		foreach ($result as $raamat) {
			//to do küsida ka autorid ja isbn
			$raamatud[] = $raamat;
		}
	} else {
		$errors[] = "Ei ole ühtegi ISBN-i registreeritud.";
	}
	include_once('views/raamat.html');
}

function taotleisbn() {
	//kas on admin või mõne kirjastaja esindaja
	global $connection;
	//kui pole sisselogitud kasutaja, siis suunab login lehele
	if (!isset($_SESSION['id'])) {
		header("Location: main.php?mode=login");
		exit();
	}
	//kontrollime, kas on admin	
	if($_SESSION['roll'] == 1) {
		//hetkel ei hakkagi peale selle teadmisega midagi
	} else {
		//kontrolli kas on seotud plokk ja kas seotud pokis on vabu numbreid
		$kirjastaja_id = $_SESSION['kirjastaja']['kirjastaja_id'];
		$plokk = getplokk($kirjastaja_id);
		if ($plokk === false) {
			$errors[] = "Pole kirjastajaplokki. Uue ploki saab anda admin.";
		} else {
			$kirjastaja['plokk'] = $plokk;
		}
	}
	if ($_SERVER['REQUEST_METHOD'] == "POST") {
		//echo "<pre>";
		//print_r($_POST);
		//echo "</pre>";
		$ok = 1;
		if (empty($_POST['pealkiri'])) {
			$errors[] = "Pealkiri tühi";
			$ok = 0;
		}
		if ($ok == 1) {
			$stmt = $connection->prepare("INSERT INTO labner_raamat (pealkiri,aasta,tyyp) VALUES (?,?,?)");
			$pealkiri = htmlspecialchars($_POST["pealkiri"]);
			$aasta = htmlspecialchars($_POST["aasta"]);
			$kirjastaja_id = htmlspecialchars($_POST["kirjastaja_id"]);
			$tyyp = $_POST["tyyp"];
			$stmt->bind_param("sis", $pealkiri,$aasta,$tyyp);
			$stmt->execute() or die ("raamat feilis");
			$raamat_id = mysqli_insert_id($connection);
			foreach ($_POST["autor"] as $autor) {
				if (!empty($autor['perenimi'])) {
					$stmt = $connection->prepare("INSERT INTO labner_autor (perenimi,eesnimi,roll) VALUES (?,?,?)");
					$stmt->bind_param("sss", $autor['perenimi'],$autor['eesnimi'],$autor['roll']);
					$stmt->execute() or die ("autor feilis");
					$autor_id = mysqli_insert_id($connection);
					$stmt = $connection->prepare("INSERT INTO labner_raamat_autor (raamat_id,autor_id) VALUES (?,?)");
					$stmt->bind_param("ii", $raamat_id,$autor_id);
					$stmt->execute() or die ("raamat_autor feilis");
				}
			}
			$stmt = $connection->prepare("INSERT INTO labner_raamat_kirjastaja (raamat_id,kirjastaja_id) VALUES (?,?)");
			$stmt->bind_param("ii", $raamat_id, $kirjastaja_id);
			$stmt->execute() or die ("raamat_kirjastaja feilis");
			$isbnstring = getisbnstring($plokk,$raamat_id);
			//kontrollib uuesti vaba ploki olemasolu
			$plokk = getplokk($kirjastaja_id);
			if ($plokk === false) {
				$errors[] = "Pole kirjastajaplokki. Uue ploki saab anda admin.";
				unset($kirjastaja['plokk']);
			} 
		}
	}
	include_once('views/isbn.html');	
}

function getisbnstring($plokk,$raamat_id) {
	global $connection;
	//vaata, milline on selle ploki viimati sisestatud raamatu tunnus
	$stmt = $connection->prepare("SELECT max(raamat) as maxrmt FROM labner_isbn WHERE plokk=?");
	$stmt->bind_param("i", $plokk); //i - int
	$stmt->execute() or die ("Ei saanud max raamatut.");
	$result = $stmt->get_result();
	$row =mysqli_fetch_assoc($result);
	//print_r($row);
	$raamat = $row['maxrmt'];
	if ($raamat === NULL) {
		//selle ploki esimene
		$raamat = 0;
	} else {
		//inkrementeeri 1 võrra
		$raamat++;
	}
	//tuleb leida mitmekohaline number plokk e. kirjastajatunnus on
	//sellest sõltub mitmekohaline saab olla raamatutunnus
	//isbn kokku 13 numbrit, prefix+riigitunnus võtab 7 kohta, kontrollnumber võtab 1 koha,
	// kirjastustunnuse ja raamatutunnuse peale kokku jääb 5 kohta
	$plokklength = strlen($plokk);
	$rmtlength = 5 - $plokklength;
	$rmtstring = sprintf('%0'.$rmtlength.'d', $raamat);
	//genereeri kontrollnumber
	$digits = str_split("978"."9985".$plokk.$rmtstring);
	$summa = 0;
    for ($i = 0;$i < sizeof($digits); $i++){
		$d = $digits[$i];	
        if ($i%2 == 0) {
			$d *= 1;
        } else {
			$d *= 3;
        }
        $summa += $d;
    }
    $ktrlnr = (10-($summa % 10)) % 10;
    $isbnstring = "978-9985-".$plokk."-".$rmtstring."-".$ktrlnr;
	$isbnnr = preg_replace('/\D/','',$isbnstring);
	
	//sisesta uus number ja raamatu seos isbn tabelisse
	$stmt = $connection->prepare("INSERT INTO labner_isbn (plokk,raamat,isbn,isbn_string,raamat_id) VALUES (?,?,?,?,?)");
	$stmt->bind_param("iiisi", $plokk,$raamat,$isbnnr,$isbnstring,$raamat_id);
	$stmt->execute() or die ("isbn päring feilis");
	
	//kui tegemist on antud ploki viimase numbriga, siis muuda current väärtust plokk_kirjastaja tabelisse
	$stmt = $connection->prepare("SELECT count(*) as arv FROM labner_isbn WHERE plokk=?");
	$stmt->bind_param("i", $plokk); 
	$stmt->execute() or die ("isbn count päring feilis");
	$result = $stmt->get_result();
	//if (mysqli_num_rows($result) == 1) {
		$row =mysqli_fetch_assoc($result);
		$arv = $row['arv'];
		$possible = pow(10,5 - $plokklength);
		if ($arv == $possible) {
			$stmt = $connection->prepare("UPDATE labner_plokk_kirjastaja SET current=0 WHERE plokk=?");
			$stmt->bind_param("i", $plokk);
			$stmt->execute() or die ("current feilis");
		}
	//}
	//tagasta isbnstring 
	return $isbnstring;
}

function getplokk($kirjastaja_id) {
	global $connection;
	$stmt = $connection->prepare("SELECT plokk FROM labner_plokk_kirjastaja WHERE kirjastaja_id=? AND current=1");
	$stmt->bind_param("i", $kirjastaja_id);
	$stmt->execute() or die ("Ei saanud plokk-kirjastaja päringut teha.");
	$result = $stmt->get_result();
	if (mysqli_num_rows($result) == 0) {
		$plokk = false;
	} else {
		$row = mysqli_fetch_assoc($result);
		$plokk = $row['plokk'];
	}
	return $plokk;
}
function annaplokk() {
	global $connection;
	if (!isset($_SESSION['id'])) {
		header("Location: main.php?mode=login");
		exit();
	}
	//kirjastajale ploki andmine - saab teha ainult admin
	//kontrolli, kas sellise id-ga kirjastus on ikka olemas ja peaks kontrollima,
	//kas selline plokk on olemas ja kas ta on vaba jne.
	//echo "<pre>";
	//print_r($_SERVER);
	//echo "</pre>";
	if($_SESSION['roll'] == 1) {
		if ($_SERVER['REQUEST_METHOD'] == "GET") {
			//echo "<pre>";
			//print_r($_GET);
			//echo "</pre>";
			$kirjastaja = getkirjastaja(htmlspecialchars($_GET['id']));
			if ($kirjastaja) {
				$stmt = $connection->prepare("SELECT * FROM labner_plokid");
				$stmt->execute() or die ("Ei saanud kirjastaja päringut teha.");
				$result = $stmt->get_result();
				foreach ($result as $vahemik) {
					$vahemikud[] = $vahemik;
				}
				include_once('views/plokk.html');
			} else {
				$error[] = "Pole sellist kirjastajat";
			}
		}
		if ($_SERVER['REQUEST_METHOD'] == "POST") {
			//sisestab ploki ja läheb kirjastaja vaatesse
			//print_r($_POST);
			$kirjastaja_id = htmlspecialchars($_POST['kirjastaja_id']);
			$plokk = htmlspecialchars($_POST['plokk']);
			seoplokk($kirjastaja_id,$plokk);
			updatevahemik($plokk);
			header("Location: main.php?mode=kirjastaja");
			exit();
		}
	} else {
		$error[] = "Plokke saab anda ainult admin kasutaja";
	}
}

function seoplokk($kirjastaja_id,$plokk) {
	global $connection;
	$stmt = $connection->prepare("INSERT INTO labner_plokk_kirjastaja (plokk,kirjastaja_id,current) VALUES (?,?,1)");
	//echo "$plokk on $kirjastaja_id on";
	$stmt->bind_param("ii", $plokk,$kirjastaja_id); //i-integer
	$stmt->execute() or die ("feilis");
}

function updatevahemik($plokk) {
	global $connection;
	$stmt = $connection->prepare("UPDATE labner_plokid SET vabad=?,next=? WHERE first <= ? AND last >= ?");
	$vabad = getvabad($plokk);
	$vabad--;
	if ($vabad == 0) {
		$next = NULL;
	} else {
		$next = $plokk + 1;
	}
	//echo " $vabad $next $plokk $plokk";
	$stmt->bind_param("iiii", $vabad,$next,$plokk,$plokk); //s-string, i-integer
	$stmt->execute() or die ("update feilis");
}

function getvabad($plokk) {
	global $connection;
	$stmt = $connection->prepare("SELECT vabad FROM labner_plokid WHERE first <= ? AND last >= ?");
	$stmt->bind_param("ii", $plokk,$plokk); 
	$stmt->execute() or die ("Ei saanud vahemiku päringut teha.");
	$result = $stmt->get_result();
	if (mysqli_num_rows($result) == 1) {
		$row =mysqli_fetch_assoc($result);
		$vabad = $row['vabad'];
		return $vabad;
	}
}

function kirjastaja() {
	global $connection;
	if (!isset($_SESSION['id'])) {
		header("Location: main.php?mode=login");
		exit();
	}
	//millise kirjastajaga seotud on
	$stmt = $connection->prepare("SELECT * FROM labner_kirjastaja");
	$stmt->execute() or die ("Ei saanud kirjastaja päringut teha.");
	$result = $stmt->get_result();
	foreach ($result as $kirjastaja) {
		//echo "midagi tuli";
		$plokk = getplokk($kirjastaja['id']);
		if ($plokk === false) {
			//echo "pole plokki";
			$kirjastaja['plokk'] = $plokk;
		} else {
			//echo "plokk $plokk";
			$kirjastaja['plokk'] = $plokk;
		}
		$kirjastajad[] = $kirjastaja;
	}
	//print_r($kirjastajad);
	//kui on admin, siis saab uue kirjastaja lisada. koos seotud kasutajaga
	include_once('views/kirjastajad.html');
}
function uuskirjastaja() {
	global $connection;
	//print_r($_SESSION);
	if (!isset($_SESSION['id'])) {
		header("Location: main.php?mode=login");
		exit();
	}
	if ($_SESSION['roll'] != 1) {
		header("Location: main.php");
		exit();
	}
	if ($_SERVER['REQUEST_METHOD'] == "POST") {
		$ok = 1;
		if (empty($_POST['kirjastajanimi'])) {$errors[] = "Kirjastajanimi tühi"; $ok = 0;}
		if (empty($_POST['kontakt'])) {$errors[] = "Kontakt tühi"; $ok = 0;}
		if (empty($_POST['kasutajanimi'])) {$errors[] = "Kasutajanimi tühi"; $ok = 0;}
		if (empty($_POST['parool'])) {$errors[] = "Parool tühi"; $ok = 0;}		
		if ($ok == 1) {
			$stmt = $connection->prepare("INSERT INTO labner_kirjastaja (kirjastaja,kontakt) VALUES (?,?)");
			$kirjastajanimi = htmlspecialchars($_POST["kirjastajanimi"]);
			$kontakt = htmlspecialchars($_POST["kontakt"]);
			$stmt->bind_param("ss", $kirjastajanimi,$kontakt);
			$stmt->execute() or die ("kirjastaja feilis");
			$kirjastaja_id = mysqli_insert_id($connection);
			$eesnimi = htmlspecialchars($_POST["eesnimi"]);
			$perenimi = htmlspecialchars($_POST["perenimi"]);
			$stmt = $connection->prepare("INSERT INTO labner_kasutaja (kasutajanimi,parool,perenimi,eesnimi) VALUES (?,SHA1(?),?,?)");
			$kasutajanimi = htmlspecialchars($_POST["kasutajanimi"]);
			$parool = htmlspecialchars($_POST["parool"]);
			//echo " $kasutajanimi,$kontakt,$perenimi,$eesnimi ";
			$stmt->bind_param("ssss", $kasutajanimi,$kontakt,$perenimi,$eesnimi);
			$stmt->execute() or die ("kasutaja feilis");
			$kasutaja_id = mysqli_insert_id($connection);
			//echo "kirjastaja ja kasutaja $kirjastaja_id $kasutaja_id";
			$stmt = $connection->prepare("INSERT INTO labner_kirjastaja_kasutaja (kirjastaja_id,kasutaja_id) VALUES (?,?)");
			$stmt->bind_param("ss", $kirjastaja_id,$kasutaja_id);
			$stmt->execute() or die ("kirjastaja_kasutaja feilis");
			//natuke kole
			unset($kirjastajanimi);
			unset($kontakt);
			unset($kasutajanimi);
			unset($parool);
			unset($kontakt);
			unset($perenimi);
			unset($eesnimi);
			header("Location: main.php?mode=annaplokk&id=".$kirjastaja_id);
			exit();
		}
	}
	include_once('views/uuskirjastaja.html');
}

?>