-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Loomise aeg: Juuni 01, 2016 kell 11:36 PM
-- Serveri versioon: 5.5.49-0ubuntu0.14.04.1
-- PHP versioon: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Andmebaas: `test`
--

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `labner_kirjastaja_kasutaja`
--

CREATE TABLE IF NOT EXISTS `labner_kirjastaja_kasutaja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kirjastaja_id` int(11) NOT NULL,
  `kasutaja_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Andmete tõmmistamine tabelile `labner_kirjastaja_kasutaja`
--

INSERT INTO `labner_kirjastaja_kasutaja` (`id`, `kirjastaja_id`, `kasutaja_id`) VALUES
(1, 1, 1),
(2, 2, 3),
(3, 3, 4),
(4, 9, 8),
(5, 10, 9),
(6, 11, 10),
(7, 12, 11),
(8, 13, 12),
(9, 14, 13),
(10, 15, 14),
(11, 16, 15);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
