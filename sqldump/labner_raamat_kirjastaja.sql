-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Loomise aeg: Juuni 01, 2016 kell 11:36 PM
-- Serveri versioon: 5.5.49-0ubuntu0.14.04.1
-- PHP versioon: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Andmebaas: `test`
--

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `labner_raamat_kirjastaja`
--

CREATE TABLE IF NOT EXISTS `labner_raamat_kirjastaja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `raamat_id` int(11) NOT NULL,
  `kirjastaja_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=61 ;

--
-- Andmete tõmmistamine tabelile `labner_raamat_kirjastaja`
--

INSERT INTO `labner_raamat_kirjastaja` (`id`, `raamat_id`, `kirjastaja_id`) VALUES
(33, 48, 1),
(34, 49, 1),
(35, 50, 1),
(36, 51, 1),
(37, 52, 1),
(38, 53, 1),
(39, 54, 14),
(40, 55, 14),
(41, 56, 14),
(42, 57, 14),
(43, 58, 14),
(44, 59, 14),
(45, 60, 14),
(46, 61, 14),
(47, 62, 14),
(50, 65, 14),
(51, 66, 14),
(52, 67, 14),
(53, 68, 14),
(54, 69, 14),
(55, 70, 14),
(56, 71, 14),
(57, 72, 14),
(58, 73, 14),
(59, 74, 14),
(60, 75, 14);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
