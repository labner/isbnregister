-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Loomise aeg: Juuni 01, 2016 kell 11:36 PM
-- Serveri versioon: 5.5.49-0ubuntu0.14.04.1
-- PHP versioon: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Andmebaas: `test`
--

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `labner_raamat_autor`
--

CREATE TABLE IF NOT EXISTS `labner_raamat_autor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `raamat_id` int(11) NOT NULL,
  `autor_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=76 ;

--
-- Andmete tõmmistamine tabelile `labner_raamat_autor`
--

INSERT INTO `labner_raamat_autor` (`id`, `raamat_id`, `autor_id`) VALUES
(71, 49, 71),
(72, 49, 72),
(73, 51, 73),
(74, 51, 74),
(75, 53, 75);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
