-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Loomise aeg: Juuni 01, 2016 kell 11:33 PM
-- Serveri versioon: 5.5.49-0ubuntu0.14.04.1
-- PHP versioon: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Andmebaas: `test`
--

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `labner_plokid`
--

CREATE TABLE IF NOT EXISTS `labner_plokid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prefix` int(11) NOT NULL DEFAULT '978',
  `maa` int(11) NOT NULL DEFAULT '9985',
  `first` int(11) NOT NULL,
  `last` int(11) NOT NULL,
  `maht` int(11) NOT NULL,
  `vabad` int(11) NOT NULL,
  `next` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Andmete tõmmistamine tabelile `labner_plokid`
--

INSERT INTO `labner_plokid` (`id`, `prefix`, `maa`, `first`, `last`, `maht`, `vabad`, `next`) VALUES
(1, 978, 9985, 0, 4, 10000, 3, 2),
(2, 978, 9985, 50, 79, 1000, 28, 52),
(3, 978, 9985, 800, 899, 100, 100, 800),
(4, 978, 9985, 9000, 9999, 10, 996, 9004);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
