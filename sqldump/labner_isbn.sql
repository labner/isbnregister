-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Loomise aeg: Juuni 01, 2016 kell 11:37 PM
-- Serveri versioon: 5.5.49-0ubuntu0.14.04.1
-- PHP versioon: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Andmebaas: `test`
--

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `labner_isbn`
--

CREATE TABLE IF NOT EXISTS `labner_isbn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plokk` int(11) NOT NULL,
  `raamat` int(11) NOT NULL,
  `isbn` bigint(20) NOT NULL,
  `isbn_string` varchar(30) NOT NULL,
  `raamat_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=70 ;

--
-- Andmete tõmmistamine tabelile `labner_isbn`
--

INSERT INTO `labner_isbn` (`id`, `plokk`, `raamat`, `isbn`, `isbn_string`, `raamat_id`) VALUES
(42, 9000, 0, 9789985900000, '978-9985-9000-0-0', 48),
(43, 9000, 1, 9789985900017, '978-9985-9000-1-7', 49),
(44, 9000, 2, 9789985900024, '978-9985-9000-2-4', 50),
(45, 9000, 3, 9789985900031, '978-9985-9000-3-1', 51),
(46, 9000, 4, 9789985900048, '978-9985-9000-4-8', 52),
(47, 9000, 5, 9789985900055, '978-9985-9000-5-5', 53),
(48, 9002, 0, 9789985900208, '978-9985-9002-0-8', 54),
(49, 9002, 1, 9789985900215, '978-9985-9002-1-5', 55),
(50, 9002, 2, 9789985900222, '978-9985-9002-2-2', 56),
(51, 9002, 3, 9789985900239, '978-9985-9002-3-9', 57),
(52, 9002, 4, 9789985900246, '978-9985-9002-4-6', 58),
(53, 9002, 5, 9789985900253, '978-9985-9002-5-3', 59),
(54, 9002, 6, 9789985900260, '978-9985-9002-6-0', 60),
(55, 9002, 7, 9789985900277, '978-9985-9002-7-7', 61),
(56, 9002, 8, 9789985900284, '978-9985-9002-8-4', 62),
(59, 9002, 9, 9789985900291, '978-9985-9002-9-1', 65),
(60, 9003, 0, 9789985900307, '978-9985-9003-0-7', 66),
(61, 9003, 1, 9789985900314, '978-9985-9003-1-4', 67),
(62, 9003, 2, 9789985900321, '978-9985-9003-2-1', 68),
(63, 9003, 3, 9789985900338, '978-9985-9003-3-8', 69),
(64, 9003, 4, 9789985900345, '978-9985-9003-4-5', 70),
(65, 9003, 5, 9789985900352, '978-9985-9003-5-2', 71),
(66, 9003, 6, 9789985900369, '978-9985-9003-6-9', 72),
(67, 9003, 7, 9789985900376, '978-9985-9003-7-6', 73),
(68, 9003, 8, 9789985900383, '978-9985-9003-8-3', 74),
(69, 9003, 9, 9789985900390, '978-9985-9003-9-0', 75);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
