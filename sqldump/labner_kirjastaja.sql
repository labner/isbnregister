-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Loomise aeg: Juuni 01, 2016 kell 11:36 PM
-- Serveri versioon: 5.5.49-0ubuntu0.14.04.1
-- PHP versioon: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Andmebaas: `test`
--

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `labner_kirjastaja`
--

CREATE TABLE IF NOT EXISTS `labner_kirjastaja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kirjastaja` varchar(200) COLLATE utf8_estonian_ci NOT NULL,
  `kontakt` varchar(200) COLLATE utf8_estonian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_estonian_ci AUTO_INCREMENT=17 ;

--
-- Andmete tõmmistamine tabelile `labner_kirjastaja`
--

INSERT INTO `labner_kirjastaja` (`id`, `kirjastaja`, `kontakt`) VALUES
(1, 'Koer ja Kuu', 'koer@kuu.ee'),
(2, 'Kolmas Silm', 'kolmas@silm.ee'),
(3, 'Viies Ratas', 'viies@ratas.ee'),
(6, 'Õunapuu ja Pojad', 'puu@pojad.ee'),
(8, 'Hiired', 'hiired@hiired.ee'),
(9, 'Tööõigus', 'too@oigus.ee'),
(11, 'Füüsikaklubi', 'fyysika@klubi.ee'),
(13, 'литература', 'lit@lit.ru'),
(14, 'Aed ja Kodu', 'aed@kodu.ee'),
(15, 'Tehnikakirjastus', 'tehnika@kirjastus.ee'),
(16, '&lt;b&gt;whatever&lt;/b&gt;', 'what@ever.ee');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
