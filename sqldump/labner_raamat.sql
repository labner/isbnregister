-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Loomise aeg: Juuni 01, 2016 kell 11:35 PM
-- Serveri versioon: 5.5.49-0ubuntu0.14.04.1
-- PHP versioon: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Andmebaas: `test`
--

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `labner_raamat`
--

CREATE TABLE IF NOT EXISTS `labner_raamat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pealkiri` varchar(250) COLLATE utf8_estonian_ci NOT NULL,
  `aasta` year(4) NOT NULL,
  `tyyp` enum('paber','epub') COLLATE utf8_estonian_ci NOT NULL,
  `thumb` varchar(20) COLLATE utf8_estonian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_estonian_ci AUTO_INCREMENT=76 ;

--
-- Andmete tõmmistamine tabelile `labner_raamat`
--

INSERT INTO `labner_raamat` (`id`, `pealkiri`, `aasta`, `tyyp`, `thumb`) VALUES
(48, 'Koer', 2016, 'paber', ''),
(49, 'Labradorid', 2016, 'epub', ''),
(50, 'Labradorid LabradoridLabradoridLabradorid LabradoridLabradoridLabradoridLabradoridLabradoridLabradoridLabradoridLabradoridLabradorid LabradoridLabradoridLabradoridLabradoridLabradoridLabradoridLabradoridLabradoridLabradoridLabradorid LabradoridLabrad', 2016, 'paber', ''),
(51, 'Kümme salapärast käpajälge. Kriminaalromaan.', 2017, 'epub', ''),
(52, 'Bernhardiinid', 2016, 'epub', ''),
(53, 'Taksid', 2016, 'epub', ''),
(54, 'Aed 1', 2016, 'paber', ''),
(55, 'Aed 2', 2016, 'paber', ''),
(56, 'Aed 3', 2016, 'paber', ''),
(57, 'Aed 4', 2016, 'paber', ''),
(58, 'Aed 5', 2016, 'paber', ''),
(59, 'Aed 6', 2016, 'paber', ''),
(60, 'Aed 7', 2016, 'paber', ''),
(61, 'Aed 8', 2016, 'paber', ''),
(62, 'Aed 9', 2016, 'paber', ''),
(65, 'Aed 100', 2016, 'paber', ''),
(66, 'Aed 21', 2016, 'paber', ''),
(67, 'Aed 21', 2016, 'paber', ''),
(68, 'Aed 21', 2016, 'paber', ''),
(69, 'Aed 21', 2016, 'paber', ''),
(70, 'Aed 21', 2016, 'paber', ''),
(71, 'Aed 21', 2016, 'paber', ''),
(72, 'Aed 21', 2016, 'paber', ''),
(73, 'Aed 21', 2016, 'paber', ''),
(74, 'Aed 21', 2016, 'paber', ''),
(75, 'Aed 21', 2016, 'paber', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
