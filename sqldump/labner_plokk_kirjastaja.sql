-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Loomise aeg: Juuni 01, 2016 kell 11:35 PM
-- Serveri versioon: 5.5.49-0ubuntu0.14.04.1
-- PHP versioon: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Andmebaas: `test`
--

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `labner_plokk_kirjastaja`
--

CREATE TABLE IF NOT EXISTS `labner_plokk_kirjastaja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plokk` int(11) NOT NULL,
  `current` tinyint(1) NOT NULL,
  `kirjastaja_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `plokk` (`plokk`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Andmete tõmmistamine tabelile `labner_plokk_kirjastaja`
--

INSERT INTO `labner_plokk_kirjastaja` (`id`, `plokk`, `current`, `kirjastaja_id`) VALUES
(18, 9000, 1, 1),
(19, 9001, 1, 2),
(20, 1, 1, 6),
(21, 50, 1, 15),
(22, 51, 1, 16),
(23, 9002, 0, 14),
(24, 9003, 0, 14);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
