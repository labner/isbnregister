-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Loomise aeg: Juuni 01, 2016 kell 11:37 PM
-- Serveri versioon: 5.5.49-0ubuntu0.14.04.1
-- PHP versioon: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Andmebaas: `test`
--

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `labner_autor`
--

CREATE TABLE IF NOT EXISTS `labner_autor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `perenimi` varchar(50) CHARACTER SET utf8 COLLATE utf8_estonian_ci NOT NULL,
  `eesnimi` varchar(50) CHARACTER SET utf8 COLLATE utf8_estonian_ci NOT NULL,
  `roll` varchar(50) CHARACTER SET utf8 COLLATE utf8_estonian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=76 ;

--
-- Andmete tõmmistamine tabelile `labner_autor`
--

INSERT INTO `labner_autor` (`id`, `perenimi`, `eesnimi`, `roll`) VALUES
(71, 'Labrador', 'Leo', 'autor'),
(72, 'Labrador', 'Lea', 'illustraator'),
(73, 'Käpp', 'Kärt', 'autor'),
(74, 'Kuri', 'Kuno', 'illustraator'),
(75, 'Taks', 'Tarmo', 'autor');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
