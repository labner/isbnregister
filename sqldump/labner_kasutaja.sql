-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Loomise aeg: Juuni 01, 2016 kell 11:37 PM
-- Serveri versioon: 5.5.49-0ubuntu0.14.04.1
-- PHP versioon: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Andmebaas: `test`
--

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `labner_kasutaja`
--

CREATE TABLE IF NOT EXISTS `labner_kasutaja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kasutajanimi` varchar(10) COLLATE utf8_estonian_ci NOT NULL,
  `parool` varchar(40) COLLATE utf8_estonian_ci NOT NULL,
  `perenimi` varchar(20) COLLATE utf8_estonian_ci NOT NULL,
  `eesnimi` varchar(20) COLLATE utf8_estonian_ci NOT NULL,
  `roll` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `kasutajanimi` (`kasutajanimi`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_estonian_ci AUTO_INCREMENT=16 ;

--
-- Andmete tõmmistamine tabelile `labner_kasutaja`
--

INSERT INTO `labner_kasutaja` (`id`, `kasutajanimi`, `parool`, `perenimi`, `eesnimi`, `roll`) VALUES
(1, 'koer', '124d7889b68349c07ed3268c31cad30de5165daf', 'Šarikov', 'Barbos', 0),
(2, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Administrator', 'Administratovich', 1),
(3, 'kolmas', '2adfa6e44ccb0150fed0c0175c8e661d42a3039f', 'Silm', 'Siim', 0),
(4, 'viies', '1301667f681752a8f266ad32ed32a735d7bafc13', 'Ratas', 'Vilma', 0),
(5, 'puu', '13e9b243fa46663038e49c94737c9c5f96ee9137', 'Õun', 'Õnne', 0),
(7, 'hiired', 'c22893936139806d90d035e7ab2c3051960da8c2', 'Hiir', 'Erni', 0),
(8, 'tooline', '9e96b886936d8d59515b94dc0de37bf01b834a29', 'Õigus', 'Tõnu', 0),
(9, '', 'da39a3ee5e6b4b0d3255bfef95601890afd80709', '', '', 0),
(10, 'fyysika', '4f95d7e3ab55ac6003e040ccdcd06a5b60908f18', '', '', 0),
(11, 'literaat', '2adeae2c4d8a1b63ea46c75f54ccac429c815a33', '', '', 0),
(12, 'litera', '4788c1ee2f890a30b3218d9999d67beef27aa385', '', '', 0),
(13, 'aednik', 'c09691540b4b8b27e1e6c8117e92e55cecfd6c9e', '', '', 0),
(14, 'tehnika', '61bfeff8e58d9d2eea93a89876b462d0fa1165e6', 'Tehnik', 'Tanja', 0),
(15, 'what', 'de1a960b31901c4b1b7ddf161a0ef7cfd5526ac7', '', '', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
