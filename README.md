# README #

Liina Abner DK14
### I244 projekt - ISBN registri prototüüp ###

ISBN (International Standard Book Number) on rahvusvahelise ISBN agentuuri poolt hallatav standard.

Number on 13-kohaline ja koosneb viiest osast: prefix-rühmatunnus-kirjastustunnus-raamatutunnus-kontrollnumber.
ISBN vahemikud iga riigi jaoks saab siit https://www.isbn-international.org/export_rangemessage.xml

Riigile kuuluvaid kirjastustunnuseid (kirjastajaplokke) jagab kirjastajatele iga riigi vastav agentuur.

978-9985 - prefix ja Eesti rühmatunnus (riigitunnus)

Eestile eraldatud kirjastustunnused (kirjastajaplokid) on:

 0-4

 50-79

 800-899

 9000-9999


##Põhifunktsioonid##
* Administraatori rollis kasutaja saab teha uusi kirjastajaid ja neile kirjastajaplokke omistada.
* Süsteem peab arvestust mitu plokki igast vahemikust vabad on.
* Kirjastaja rollis kasutaja saab oma ploki seest isbn numbreid küsida.
* Rakendus arvutab kontrollnumbri.
* Rakendus peab arvestust, mitu numbrit plokis vabad on. Kui plokk täis saab, peab admin kasutaja kirjastajale uue ploki andma.

##Abiks retsenseerijale##
* Põhitegevus toimub php-s ja mysql-is.
* Andmete sisestamisel kontrollib, kas kohustuslikud (roosad)väljad on täidetud. Juba sisestatud andmed jäävad alles.
* Javascripti on ka natuke - isbn taotlusvormil on nupp, mis tekitab autorivälju juurde
* css-i highlight on w3 näitel tehtud tooltip raamatute nimekirja liiga pikkade pealkirjade kohal
* live demo http://enos.itcollege.ee/~labner/isbnregister
* Testida saab kasutajaga admin/admin ja koer/koer.

Enjoy!